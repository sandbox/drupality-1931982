<?php

class TestDataCreator {
  public static $nids = array();
  public static $mlids = array();
  public static $menu_name = '';
  public static $uid;
  public static $rid;

  public static function create() {
    self::setDbConnection();

    if (!module_exists('menu_perm')) {
      module_enable(array('menu_perm'));
    }


    self::$menu_name = self::createTestMenu();
    variable_set('menu_options_article', array(self::$menu_name, 'main-menu'));
    variable_set('menu_perm_content_types', array('article'));
    variable_set('menu_perm_menus', array(self::$menu_name));
    $role = self::createTestRole();
    self::$rid = $role->rid;
    self::$uid = self::createTestUser($role);
    variable_set('menu_perm_user_roles', array($role->name));
  }

  public static function cleanUp() {

    node_delete_multiple(self::$nids);

    menu_delete(array(
      'menu_name' => self::$menu_name,
      'title' => 'Test menu',
      'description' => ''
    ));

    user_delete(self::$uid);
    user_role_delete(self::$rid);


  }


  /**
   * - Item1
   * -- Item1.1
   * -- Item1.2
   * -- Item1.3
   * ---Item1.3.1
   * ---Item1.3.2
   * -Item2
   * -Item3
   * --Item3.1
   * --Item3.2
   * --Item3.3
   * -Item4
   * --Item4.1
   * ---Item4.1.1
   * ----Item4.1.1.1
   * ----Item4.1.1.2
   * ----Item4.1.1.3
   * --Item4.2
   * --Item4.3
   * ---Item4.3.1
   * ---Item4.3.2
   * --Item4.4
   * -Item5
   * -Item6
   */
  private static function createTestMenu() {
    $menu_name = 'test_menu';
    if (menu_load($menu_name)) {
      return $menu_name;
    }
    menu_save(array(
      'menu_name' => $menu_name,
      'title' => 'Test menu',
      'description' => '',
    ));
    $menu_items = array(
      'Item1' => array(
        'name' => 'Item1',
        'mlid' => FALSE,
        'childs' =>
        array(
          'Item1.1' => array(
            'name' => 'Item1.1',
            'mlid' => FALSE,
            'plid' => FALSE,
          ),
          'Item1.2' => array(
            'name' => 'Item1.2',
            'mlid' => FALSE,
            'plid' => FALSE,
          ),
          'Item1.3' => array(
            'name' => 'Item1.3',
            'mlid' => FALSE,
            'plid' => FALSE,
            'childs' => array(
              'Item1.3.1' => array(
                'name' => 'Item1.3.1',
                'mlid' => FALSE,
                'plid' => FALSE,
              ),
              'Item1.3.2' => array(
                'name' => 'Item1.3.2',
                'mlid' => FALSE,
                'plid' => FALSE,
              ),
            )
          ),
        )
      ),
      'Item2' => array(
        'name' => 'Item2',
        'mlid' => FALSE,
      ),
      'Item3' => array(
        'name' => 'Item3',
        'mlid' => FALSE,
        'childs' => array(
          'Item3.1' => array(
            'name' => 'Item3.1',
            'mlid' => FALSE,
            'plid' => FALSE,
          ),
          'Item3.2' => array(
            'name' => 'Item3.2',
            'mlid' => FALSE,
            'plid' => FALSE,
          ),
          'Item3.3' => array(
            'name' => 'Item3.3',
            'mlid' => FALSE,
            'plid' => FALSE,
          ),
        ),
      ),
      'Item4' => array(
        'name' => 'Item4',
        'mlid' => FALSE,
        'childs' => array(
          'Item4.1' => array(
            'name' => 'Item4.1',
            'mlid' => FALSE,
            'plid' => FALSE,
            'childs' => array(
              'Item4.1.1' => array(
                'name' => 'Item4.1.1',
                'mlid' => FALSE,
                'plid' => FALSE,
                'childs' => array(
                  'Item4.1.1.1' => array(
                    'name' => 'Item4.1.1.1',
                    'mlid' => FALSE,
                    'plid' => FALSE,
                  ),
                  'Item4.1.1.2' => array(
                    'name' => 'Item4.1.1.2',
                    'mlid' => FALSE,
                    'plid' => FALSE,
                  ),
                  'Item4.1.1.3' => array(
                    'name' => 'Item4.1.1.3',
                    'mlid' => FALSE,
                    'plid' => FALSE,
                  ),
                )
              )
            )
          ),
          'Item4.2' => array(
            'name' => 'Item4.2',
            'mlid' => FALSE,
            'plid' => FALSE,
          ),
          'Item4.3' => array(
            'name' => 'Item4.3',
            'mlid' => FALSE,
            'plid' => FALSE,
            'childs' => array(
              'Item4.3.1' => array(
                'name' => 'Item4.3.1',
                'mlid' => FALSE,
                'plid' => FALSE,
              ),
              'Item4.3.2' => array(
                'name' => 'Item4.3.2',
                'mlid' => FALSE,
                'plid' => FALSE,
              ),
            )
          ),
          'Item4.4' => array(
            'name' => 'Item4.4',
            'mlid' => FALSE,
            'plid' => FALSE,
          )
        )
      ),
      'Item5' => array(
        'name' => 'Item5',
        'mlid' => FALSE,
        'plid' => FALSE,
      ),
      'Item6' => array(
        'name' => 'Item6',
        'mlid' => FALSE,
        'plid' => FALSE,
      )
    );
    self::createTestMenuItems($menu_items);
    menu_rebuild();
    return $menu_name;
  }

  private static function createTestMenuItems($items, $plid = 0) {
    foreach ($items as $item) {
      if ($plid) {
        $item['plid'] = $plid;
      }
      $mlid = self::createTestMenuItem($item);
      if (isset($item['childs'])) {
        self::createTestMenuItems($item['childs'], $mlid);
      }
    }
  }

  private static function createTestMenuItem($item_data) {
    $item['menu_name'] = 'test_menu';
    $item['link_title'] = $item_data['name'];
    $item['link_path'] = 'node/' . self::createTestContent($item['link_title']);
    if (isset($item_data['plid'])) {
      $item['plid'] = $item_data['plid'];
    }
    $mlid = menu_link_save($item);
    self::$mlids[$item_data['name']] = $mlid;
    return $mlid;
  }

  private static function createTestContent($title) {
    $node = new stdClass;
    $node->type = 'article';
    node_object_prepare($node);
    $node->title = $title;
    $node->language = LANGUAGE_NONE;
    $node->body[$node->language][0]['value'] = '';
    $node->body[$node->language][0]['summary'] = '';
    $node->body[$node->language][0]['format'] = 'filtered_html';
    node_save($node);

    self::$nids[] = $node->nid;
    return $node->nid;
  }

  private static function createTestUser($role) {
    if (($user = user_load_by_name('foo'))) {
      return $user->uid;
    }
    $user = new stdClass();
    $user->name = 'foo';
    $user->pass = 'pass'; // plain text, hashed later
    $user->mail = 'john@doe.com';
    $user->roles = array(
      DRUPAL_AUTHENTICATED_RID => 'authenticated user',
      $role->rid => $role->name,
    );
    $user->status = 1; // omit this line to block this user at creation
    $user->is_new = TRUE; // not necessary because we already omit $new_user->uid
    $user = user_save($user);
    return $user->uid;
  }

  private static function createTestRole() {
    $role_name = 'editor';
    if (($editor_role = user_role_load_by_name($role_name))) {
      return $editor_role;
    }
    $role = new stdClass;
    $role->name = $role_name;
    user_role_save($role);
    $editor_role = user_role_load_by_name($role_name);
    $editor_permissions = array(
      'create article content' => TRUE,
      'administer menu' => TRUE,
    );
    user_role_change_permissions($editor_role->rid, $editor_permissions);
    return $editor_role;
  }

  /**
   * I am using multisite env so this sets proper database
   */
  private static function setDbConnection() {
    $other_database = array(
      'database' => 'base7.menu_perm',
      'username' => 'root', // assuming this is necessary
      'password' => 'pass', // assuming this is necessary
      'host' => 'localhost', // assumes localhost
      'driver' => 'mysql', // replace with your database driver
    );
    // replace 'YourDatabaseKey' with something that's unique to your module
    Database::addConnectionInfo('menu_perm', 'default', $other_database);
    db_set_active('menu_perm');
  }
}
