<?php

require_once "PHPUnit/Autoload.php";
chdir(getcwd() . '/../../../../../../');
define('DRUPAL_ROOT', getcwd());
include_once DRUPAL_ROOT . '/includes/bootstrap.inc';
drupal_bootstrap(DRUPAL_BOOTSTRAP_FULL);


$file = DRUPAL_ROOT . '/' . drupal_get_path('module', 'menu_perm') . '/tests/TestDataCreator.php';
include DRUPAL_ROOT . '/' . drupal_get_path('module', 'menu_perm') . '/menu_perm.admin.inc';

if (is_file($file)) {
  require_once $file;
  TestDataCreator::create();
}
else {
  die('TestDataCreator class file cannot be found');
}

class MenuPermTest extends PHPUnit_Framework_TestCase {

  private $nids = array();
  private $mlids = array();
  private $menu_name;
  private $uid;
  private $rid;

  protected function setUp() {
    $this->nids = TestDataCreator::$nids;
    $this->mlids = TestDataCreator::$mlids;
    $this->menu_name = TestDataCreator::$menu_name;
    $this->uid = TestDataCreator::$uid;
    $this->rid = TestDataCreator::$rid;
  }

  public function __destruct() {
    # TestDataCreator::cleanUp();
  }

  public function testSetup() {
    $this->assertTrue(isset($this->menu_name));
    $this->assertTrue(isset($this->rid));
    $this->assertFalse(empty($this->mlids));
    $this->assertTrue(isset($this->uid));
    $this->assertTrue(is_object(user_load($this->uid)));
  }

  public function testUserScenerio() {
    $uid = $this->uid;
    $this->assertTrue(_menu_perm_checkUserIsAllowed($uid));
    $form_state = array();
    $form_state['values']['uid'] = $uid;
    $menu = $this->menu_name;
    $first = array(
      $this->mlids['Item1.2'] => $menu,
      $this->mlids['Item4.1.1'] => $menu,
      $this->mlids['Item4.1.1.2'] => $menu,
    );

    $second = array(
      $this->mlids['Item5'] => $menu,
      $this->mlids['Item4.1.1'] => $menu,
      $this->mlids['Item4.1.1.2'] => $menu,
      $this->mlids['Item4.3'] => $menu,
    );

    $third = array(
      $this->mlids['Item4.1.1'] => $menu,
      $this->mlids['Item4.1.1.2'] => $menu,
    );

    $this->_testUserPermsAreCorrectAfterSubmit($first, $form_state);
    $this->_testUserPermsAreCorrectAfterSubmit($second, $form_state);
    $this->_testUserPermsAreCorrectAfterSubmit($third, $form_state);
  }

  public function testUserIsAllowed() {
    $this->assertTrue(_menu_perm_checkUserIsAllowed($this->uid));
  }


  public function testUserHasPermission() {
    $user = user_load($this->uid);
    drupal_static_reset();
    $permission = _menu_perm_createPermissionToken($this->mlids['Item4.1.1'], $this->menu_name);
    $this->assertTrue(_menu_perm_checkUserHasPermission($permission, $user));


    $permission = _menu_perm_createPermissionToken($this->mlids['Item4.3'], $this->menu_name);
    $this->assertFalse(_menu_perm_checkUserHasPermission($permission, $user));

    $permission = _menu_perm_createPermissionToken($this->mlids['Item1.2'], $this->menu_name);
    $this->assertFalse(_menu_perm_checkUserHasPermission($permission, $user));

    $permission = _menu_perm_createPermissionToken($this->mlids['Item4.1.1.2'], $this->menu_name);
    $this->assertTrue(_menu_perm_checkUserHasPermission($permission, $user));

    $permission = _menu_perm_createPermissionToken($this->mlids['Item4.4'], $this->menu_name);
    $this->assertFalse(_menu_perm_checkUserHasPermission($permission, $user));

    $permission = _menu_perm_createPermissionToken($this->mlids['Item1.2'], $this->menu_name);
    $this->assertFalse(_menu_perm_checkUserHasPermission($permission, $user));

  }

  private function _testUserPermsAreCorrectAfterSubmit($selected_perms, &$form_state) {
    $submit_perms = $this->createPermItems($selected_perms);
    $form_state['values']['items_' . $this->menu_name] = $submit_perms;
    menu_perm_user_form_submit(NULL, $form_state);
    drupal_static_reset();
    $saved_perms = _menu_perm_getAllTokensByUid($this->uid);
    $this->assertEquals(sort(array_values($submit_perms)), sort(array_values($saved_perms)));
  }

  private function createPermItems($selected_perms) {
    $perm_values = array();
    foreach ($selected_perms as $mlid => $menu) {
      $token = _menu_perm_createPermissionToken($mlid, $menu);
      $perm_values[$token] = $token;
    }
    return $perm_values;
  }

  /**
   * - Item1
   * -- Item1.1
   * -- Item1.2
   * -- Item1.3
   * ---Item1.3.1
   * ---Item1.3.2
   * -Item2
   * -Item3
   * --Item3.1
   * --Item3.2
   * --Item3.3
   * -Item4
   * --Item4.1
   * ---Item4.1.1
   * ----Item4.1.1.1
   * ----Item4.1.1.2
   * ----Item4.1.1.3
   * --Item4.2
   * --Item4.3
   * ---Item4.3.1
   * ---Item4.3.2
   * --Item4.4
   * -Item5
   * -Item6
   */
  private function createTestMenu() {
    $menu_name = 'test_menu';
    if (menu_load($menu_name)) {
      return $menu_name;
    }
    menu_save(array(
      'menu_name' => $menu_name,
      'title' => 'Test menu',
      'description' => '',
    ));
    $menu_items = array(
      'Item1' => array(
        'name' => 'Item1',
        'mlid' => FALSE,
        'childs' =>
        array(
          'Item1.1' => array(
            'name' => 'Item1.1',
            'mlid' => FALSE,
            'plid' => FALSE,
          ),
          'Item1.2' => array(
            'name' => 'Item1.2',
            'mlid' => FALSE,
            'plid' => FALSE,
          ),
          'Item1.3' => array(
            'name' => 'Item1.3',
            'mlid' => FALSE,
            'plid' => FALSE,
            'childs' => array(
              'Item1.3.1' => array(
                'name' => 'Item1.3.1',
                'mlid' => FALSE,
                'plid' => FALSE,
              ),
              'Item1.3.2' => array(
                'name' => 'Item1.3.2',
                'mlid' => FALSE,
                'plid' => FALSE,
              ),
            )
          ),
        )
      ),
      'Item2' => array(
        'name' => 'Item2',
        'mlid' => FALSE,
      ),
      'Item3' => array(
        'name' => 'Item3',
        'mlid' => FALSE,
        'childs' => array(
          'Item3.1' => array(
            'name' => 'Item3.1',
            'mlid' => FALSE,
            'plid' => FALSE,
          ),
          'Item3.2' => array(
            'name' => 'Item3.2',
            'mlid' => FALSE,
            'plid' => FALSE,
          ),
          'Item3.3' => array(
            'name' => 'Item3.3',
            'mlid' => FALSE,
            'plid' => FALSE,
          ),
        ),
      ),
      'Item4' => array(
        'name' => 'Item4',
        'mlid' => FALSE,
        'childs' => array(
          'Item4.1' => array(
            'name' => 'Item4.1',
            'mlid' => FALSE,
            'plid' => FALSE,
            'childs' => array(
              'Item4.1.1' => array(
                'name' => 'Item4.1.1',
                'mlid' => FALSE,
                'plid' => FALSE,
                'childs' => array(
                  'Item4.1.1.1' => array(
                    'name' => 'Item4.1.1.1',
                    'mlid' => FALSE,
                    'plid' => FALSE,
                  ),
                  'Item4.1.1.2' => array(
                    'name' => 'Item4.1.1.2',
                    'mlid' => FALSE,
                    'plid' => FALSE,
                  ),
                  'Item4.1.1.3' => array(
                    'name' => 'Item4.1.1.3',
                    'mlid' => FALSE,
                    'plid' => FALSE,
                  ),
                )
              )
            )
          ),
          'Item4.2' => array(
            'name' => 'Item4.2',
            'mlid' => FALSE,
            'plid' => FALSE,
          ),
          'Item4.3' => array(
            'name' => 'Item4.3',
            'mlid' => FALSE,
            'plid' => FALSE,
            'childs' => array(
              'Item4.3.1' => array(
                'name' => 'Item4.3.1',
                'mlid' => FALSE,
                'plid' => FALSE,
              ),
              'Item4.3.2' => array(
                'name' => 'Item4.3.2',
                'mlid' => FALSE,
                'plid' => FALSE,
              ),
            )
          ),
          'Item4.4' => array(
            'name' => 'Item4.4',
            'mlid' => FALSE,
            'plid' => FALSE,
          )
        )
      ),
      'Item5' => array(
        'name' => 'Item5',
        'mlid' => FALSE,
        'plid' => FALSE,
      ),
      'Item6' => array(
        'name' => 'Item6',
        'mlid' => FALSE,
        'plid' => FALSE,
      )
    );
    $this->createTestMenuItems($menu_items);
    return $menu_name;
  }

  private function createTestMenuItems($items, $plid = 0) {
    foreach ($items as $item) {
      if ($plid) {
        $item['plid'] = $plid;
      }
      $mlid = $this->createTestMenuItem($item);
      if (isset($item['childs'])) {
        $this->createTestMenuItems($item['childs'], $mlid);
      }
    }
  }

  private function createTestMenuItem($item_data) {
    $item['menu_name'] = 'test_menu';
    $item['link_title'] = $item_data['name'];
    $item['link_path'] = 'node/' . $this->createTestContent($item['link_title']);
    if (isset($item_data['plid'])) {
      $item['plid'] = $item_data['plid'];
    }
    $mlid = menu_link_save($item);
    $this->mlid[$item_data['name']] = $mlid;
    return $mlid;
  }

  private function createTestContent($title) {
    $node = new stdClass;
    $node->type = 'article';
    node_object_prepare($node);
    $node->title = $title;
    $node->language = LANGUAGE_NONE;
    $node->body[$node->language][0]['value'] = '';
    $node->body[$node->language][0]['summary'] = '';
    $node->body[$node->language][0]['format'] = 'filtered_html';
    node_save($node);

    $this->nids[] = $node->nid;
    return $node->nid;
  }

  private function createTestUser($role) {
    if (($user = user_load_by_name('foo'))) {
      return $user->uid;
    }
    $new_user = new stdClass();
    $new_user->name = 'foo';
    $new_user->pass = 'pass'; // plain text, hashed later
    $new_user->mail = 'john@doe.com';
    $new_user->roles = array(
      DRUPAL_AUTHENTICATED_RID => 'authenticated user',
      $role->rid => $role->name,
    );
    $new_user->status = 1; // omit this line to block this user at creation
    $new_user->is_new = TRUE; // not necessary because we already omit $new_user->uid
    $new_user = user_save($new_user);
    return $user->uid;
  }

  private function createTestRole() {
    $role_name = 'menu_perm_editor';
    if (($editor_role = user_role_load_by_name($role_name))) {
      return $editor_role;
    }
    $role = new stdClass;
    $role->name = $role_name;
    user_role_save($role);
    $editor_role = user_role_load_by_name($role_name);
    $editor_permissions = array(
      'create article content' => TRUE,
      'administer menu' => TRUE,
    );
    user_role_change_permissions($editor_role->rid, $editor_permissions);
    return $editor_role;
  }


}
