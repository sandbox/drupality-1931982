<?php


/**
 * Form constructor for the module settings form.
 *
 * @ingroup forms
 */
function menu_perm_admin() {
  $form = array();

  $form['menu_perm_content_types'] = array(
    '#type' => 'checkboxes',
    '#title' => t('Content types'),
    '#options' => node_type_get_names(),
    '#default_value' => variable_get('menu_perm_content_types', array()),
    '#description' => t(MENU_PERM_SELECT_CONTENT_TYPES_DESC),
  );
  $roles = user_roles(TRUE);
  $form['menu_perm_user_roles'] = array(
    '#type' => 'checkboxes',
    '#title' => t('User roles'),
    '#options' => array_combine($roles, $roles),
    '#default_value' => variable_get('menu_perm_user_roles', array()),
    '#description' => t(MENU_PERM_SELECT_USER_ROLES_DESC),
  );

  $form['menu_perm_menus'] = array(
    '#type' => 'checkboxes',
    '#title' => t('Menus'),
    '#options' => menu_get_menus(TRUE),
    '#default_value' => variable_get('menu_perm_menus', array()),
    '#description' => t(MENU_PERM_SELECT_MENUS_DESC),
  );

  return system_settings_form($form);
}


function menu_perm_user_page() {
  if (_menu_perm_checkUserIsAllowed()) {
    _menu_perm_attachCSSandJSforMenuTreeForm();
    return drupal_get_form('menu_perm_user_form');
  }
  else {
    drupal_set_message(t(MENU_PERM_ROLES_ERR_MSG), 'warning');
    drupal_goto('admin/people');
  }
}

/**
 * Form constructor for the user menu permission form.
 *
 * @see menu_perm_user_form_validate()
 * @see menu_perm_user_form_submit()
 * @ingroup forms
 */
function menu_perm_user_form() {

  $form = array();
  $uid = arg(1);
  $form['uid'] = array(
    '#type' => 'hidden',
    '#value' => $uid,

  );

  $form['help'] = array(
    '#type' => 'item',
    '#markup' => '<div class="form-help">' . t(MENU_PERM_USER_EDIT_MSG) . '</div>',

  );

  $menus = _menu_perm_getMenus();
  $menu_titles = _menu_perm_getMenusTitles(array_keys($menus));
  $current_perms = _menu_perm_getAllTokensByUid($uid);
  $counter = 0;
  foreach ($menus as $name => $items) {
    $form[$name] = array(
      '#type' => 'fieldset',
      '#title' => $menu_titles[$name],
      '#collapsible' => TRUE,
      '#collapsed' => FALSE,
    );
    $options = _menu_perm_getCheckboxesData($name, $items);

    $controls_text = array(
      t('Collapse All'),
      t('Expand All'),
    );
    $counter++;
    $tree_controls_html = <<<HTML

        <div id="treecontrol_{$counter}">
            <a href="#">{$controls_text[0]}</a>
            |
            <a href="#">{$controls_text[1]}</a>
        </div>

HTML;
    $form[$name]['items_' . $name] = array(
      '#type' => 'checkboxes',
      '#options' => $options,
      '#default_value' => array_intersect($current_perms, array_keys($options)),
      '#theme' => 'menu_perm_checkboxes',
      '#prefix' => $tree_controls_html,
      '#tree' => TRUE,
    );
  }
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save permissions'),
  );
  return $form;
}


function theme_menu_perm_checkboxes($vars) {
  static $counter = 0;
  $id = 'menu_perm_tree_' . ++$counter;
  $script = <<<JS
        (function ($) {
              $(document).ready(function() {
                var id = '#$id ul';
                 $(id).treeview({
                     animated: "fast",
                     collapsed: true,
                     control: "#treecontrol_$counter"
                 });
              });
        })(jQuery);
JS;
  drupal_add_js($script, 'inline');
  $options = $vars['checkboxes']['#options'];
  if (empty($options)) {
    drupal_set_message(t(MENU_PERM_MENU_NO_ITEMS), 'warning');
    return;
  }
  $token = _menu_perm_parsePermissionToken(array_shift(array_keys($options)));
  $menu_name = $token['menu_name'];
  $menus = _menu_perm_getMenus();
  $default_values = $vars['checkboxes']['#default_value'];
  $output = _menu_perm_renderCheckboxesList($menus[$menu_name], $menu_name, $default_values);
  return '<div id="' . $id . '" class="menu-perm-tree">' . $output . '</div>';
}

function _menu_perm_renderCheckboxesList($menu, $menu_name, $default_values) {
  $output = '<ul>';
  $item_pattern = '<li>%s<label for="%s">%s</label>';
  foreach ($menu as $item) {
    $value = _menu_perm_createPermissionToken($item['link']['mlid'], $menu_name);
    $title = check_plain($item['link']['link_title']);
    $is_checked = in_array($value, $default_values);
    $checkboxHtml = _menu_perm_renderCheckbox($value, $is_checked);
    $output .= sprintf($item_pattern, $checkboxHtml, check_plain($value), $title);
    if (!empty($item['below'])) {
      $output .= _menu_perm_renderCheckboxesList($item['below'], $menu_name, $default_values);
    }
    $output .= '</li>';
  }
  return $output .= '</ul>';
}

function _menu_perm_renderCheckbox($return_value, $is_checked = FALSE) {
  $variables = array();
  $variables['element']['#type'] = 'checkbox';
  $variables['element']['#attributes']['id'] = $return_value;
  $variables['element']['#attributes']['class'] = array('menu-perm-checkbox');
  $token = _menu_perm_parsePermissionToken($return_value);
  $name_attr_pattern = 'items_%s[%s]';
  $variables['element']['#attributes']['name'] = sprintf($name_attr_pattern, $token['menu_name'], $return_value);
  if ($is_checked) {
    $variables['element']['#attributes']['checked'] = 'checked';
  }
  $variables['element']['#return_value'] = $return_value;
  return theme('checkbox', $variables);
}

function _menu_perm_getCheckboxesData($menu_name, $items) {
  $options = array();
  foreach ($items as $item) {
    $mlid = $item['link']['mlid'];
    $id = _menu_perm_createPermissionToken($mlid, $menu_name);
    $options[$id] = $item['link']['link_title'];
    if (!empty($item['below'])) {
      $options = array_merge($options, _menu_perm_getCheckboxesData($menu_name, $item['below']));
    }
  }
  return $options;
}

/**
 * Form validation handler for menu_perm_user_form().
 *
 * @see menu_perm_user_form_submit()
 */
function menu_perm_user_form_validate($form, &$form_state) {
  if (!_menu_perm_checkUserIsAllowed($form_state['values']['uid'])) {
    form_set_error('', t(MENU_PERM_ROLES_ERR_MSG));
  }
}

/**
 * Form submission handler for menu_perm_user_form_submit).
 *
 * @see menu_perm_user_form_validate()
 */
function menu_perm_user_form_submit($form, &$form_state) {
  $uid = (int) $form_state['values']['uid'];
  $menus = _menu_perm_getMenus();
  $tokens = array();
  foreach (array_keys($menus) as $key) {
    foreach ($form_state['values']['items_' . $key] as $k => $val) {
      $tokens[] = $val;
    }
  }
  $posted_tokens = array_filter($tokens);
  $old_tokens = _menu_perm_getAllTokensByUid($uid);
  $new_tokens = array_diff($posted_tokens, $old_tokens);
  $to_delete_tokens = array_diff($old_tokens, $posted_tokens);

  if (!empty($new_tokens)) {
    $values = array();
    foreach ($new_tokens as $token) {
      $parsed = _menu_perm_parsePermissionToken($token);
      $values[] = array(
        'uid' => $uid,
        'mlid' => $parsed['mlid'],
        'menu_name' => $parsed['menu_name'],
      );
    }
    $query = db_insert(MENU_PERM_TBL)->fields(array('uid', 'mlid', 'menu_name'));
    foreach ($values as $record) {
      $query->values($record);
    }
    $query->execute();
  }

  if (!empty($to_delete_tokens)) {

    $to_delete_mlids = array();
    foreach ($to_delete_tokens as $token) {
      $parsed = _menu_perm_parsePermissionToken($token);
      $to_delete_mlids[] = $parsed['mlid'];
    }

    db_delete(MENU_PERM_TBL)
      ->condition('uid', $uid)
      ->condition('mlid', $to_delete_mlids, 'IN')
      ->execute();
  }

  drupal_set_message(t(MENU_PERM_USER_PERMS_SAVE_OK));

}

/**
 * Implements hook_user_delete
 */
function menu_perm_user_delete($account) {
  db_delete(MENU_PERM_TBL)
    ->condition('uid', $account->uid)
    ->execute();
}

function _menu_perm_attachCSSandJSforMenuTreeForm() {
  drupal_add_css(
    '
        .menu-perm-tree label { display: inline; cursor: pointer; padding-left: 5px }
        .menu-perm-tree .treeview ul { margin: 0; padding: 0; }
       ',
    array(
      'group' => CSS_THEME,
      'type' => 'inline',
      'media' => 'all',
      'preprocess' => FALSE,
      'weight' => '9999',
    )
  );
  drupal_add_css(drupal_get_path('module', 'menu_perm') . '/jquery.treeview/jquery.treeview.css');
  drupal_add_js(drupal_get_path('module', 'menu_perm') . '/jquery.treeview/jquery.treeview.min.js');

  $script = <<<JS

         (function ($) {
              $(document).ready(function() {
                 var treeLists = $(".menu-perm-tree");
                 if (treeLists.length > 0) {
                    treeLists.find(".menu-perm-checkbox")
                             .click(function() {
                                 var curr = $(this),
                                     children = curr.parent().find(".menu-perm-checkbox");
                                 if (children.length > 0) {
                                     if (curr.is(":checked")) {
                                        children.each(function() {
                                            $(this).attr("checked", true);
                                        });
                                     } else {
                                        children.each(function() {
                                            $(this).attr("checked", false);
                                        });
                                     }
                                 }
                             });
                 }
              });
        })(jQuery);
JS;

  drupal_add_js($script, 'inline');
}
